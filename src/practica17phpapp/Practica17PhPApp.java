
package practica17phpapp;


public class Practica17PhPApp {

   
    public static void main(String[] args) {
        
        Electrodomestico lavadora;
        lavadora = new Electrodomestico("Lavadora", "AEG", 20);
        System.out.println(lavadora.toString());
        System.out.println("Consumo: " + lavadora.getConsumo(20));
        System.out.println("Coste: " + lavadora.getCosteConsumo(20, 10));
        
        
        Lavadora as100;
        as100 = new Lavadora(500, true, "AEG", 20);
        System.out.println(as100.toString());
        System.out.println("Consumo: "+ as100.getConsumo(20));
         System.out.println("Coste: " + as100.getCosteConsumo(20, 10));
         
    }
    
}
