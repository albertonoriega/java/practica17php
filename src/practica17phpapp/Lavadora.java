
package practica17phpapp;


public class Lavadora extends Electrodomestico {
    
    public double precio;
    public boolean aguaCaliente;

    // Constructor
    public Lavadora(double precio, boolean aguaCaliente, String marca, double potencia) {
        super(marca, potencia);
        this.precio = precio;
        this.aguaCaliente = aguaCaliente;
    }

    // Getter Y setter
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public boolean isAguaCaliente() {
        return aguaCaliente;
    }

    public void setAguaCaliente(boolean aguaCaliente) {
        this.aguaCaliente = aguaCaliente;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getPotencia() {
        return potencia;
    }

    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }

    @Override
    public String toString() {
        return "La lavadora tiene un precio: "+ precio + "euros, aguaCaliente: " + aguaCaliente + " su potencia es: "+ getPotencia() + " y su marca es: " + getMarca();
    }

    @Override
    public double getConsumo(int horas) {
        double resultado;
        if(this.aguaCaliente = true){
            resultado= horas*(potencia+potencia*0.20);
        } else {
            resultado= horas*potencia;
        }
        return resultado;
    }

  

   
    

 

  
    
    
    
}
