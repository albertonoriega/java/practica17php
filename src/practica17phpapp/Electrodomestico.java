
package practica17phpapp;


public class Electrodomestico {
    public String tipo;
    public String marca;
    public double potencia;

    // Constructor que asigne valores a cada propiedad
    
    public Electrodomestico(String tipo, String marca, double potencia) {
        this.tipo = tipo;
        this.marca = marca;
        this.potencia = potencia;
    }

    public Electrodomestico(String marca, double potencia) {
        this.marca = marca;
        this.potencia = potencia;
    }

  
    
    //GETTER Y SETTER

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public double getPotencia() {
        return potencia;
    }

    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }

    // Metodo toString
    @Override
    public String toString() {
        return "El electrodomestico es un/una: " + tipo + " de la marca: " + marca + " y su potencia es: " + potencia + " KW por hora ";
    }
    
    // Metodo getConsumo
    
    public double getConsumo(int horas){
        return horas*this.potencia;
    }
    
    // Metodo getCosteConsumo
    
    public double getCosteConsumo (int horas , double costeHoras){
        return horas*this.potencia*costeHoras;
    }
}
